#./deployLocal.sh
#docker stop infrah2021tp5startingpoint_api_1
#docker container rm infrah2021tp5startingpoint_api_1
#docker volume rm tp5dlwp_vol
#docker image rm img_api2
#docker volume create --name tp5dlwp_vol --opt device=$PWD --opt o=bind --opt type=none
#docker build -t img_api2 -f ./project/docker/DockerfileAPI .
#docker run -e "INFRA_TP5_DB_TYPE=SQLite" -d -p 5555:5555 --mount source=tp5dlwp_vol,target=/mnt/app/ --name infrah2021tp5startingpoint_api_1 img_api2
sleep 15
echo "Running deployment tests"
python3 -m unittest discover -s ./project -v -p dtest_*.py
if [ $? -ne 0 ]; then
        echo "Deployment tests failed"
        exit 1
fi
