docker stop tp5dlwp
docker container rm tp5dlwp
docker volume rm tp5dlwp_vol
docker image rm tp5dlwp_image
docker volume create --name tp5dlwp_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp5dlwp_image -f ./project/docker/DockerfileAPI .
docker run -e "INFRA_TP5_DB_TYPE=MYSQL" -e "INFRA_TP5_DB_HOST=db-tp5-infra.ddnsgeek.com" -e "INFRA_TP5_DB_PORT=7777" -e "INFRA_TP5_DB_USER=produser" -e "INFRA_TP5_DB_PASSWORD=3ac4d0b0e24871436f45275890a458c6" -d -p 5555:5555 --mount source=tp5dlwp_vol,target=/mnt/app/ --name tp5dlwp tp5dlwp_image
